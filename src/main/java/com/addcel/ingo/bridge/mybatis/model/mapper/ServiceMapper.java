package com.addcel.ingo.bridge.mybatis.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.ingo.bridge.mybatis.model.vo.ClienteVO;


public interface ServiceMapper {

	public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
}
