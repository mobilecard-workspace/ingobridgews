package com.addcel.ingo.bridge.utils;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilsService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsService.class);
	
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			LOGGER.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			LOGGER.error("2.- Error al mapperar objeto: {}",e);
		} catch (IOException e) {
			LOGGER.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			LOGGER.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			LOGGER.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	
}

