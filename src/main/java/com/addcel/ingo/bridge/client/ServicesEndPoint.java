package com.addcel.ingo.bridge.client;

import javax.xml.ws.Endpoint;

public class ServicesEndPoint {
	
	private static final String endpoint = "https://api.spykemobile.net/"; // PROD
	//private static final String endpoint = "https://uat3.spykemobile.net:8443/"; //QA
	// QA "https://uat3.spykemobile.net:8443/";
	// PROD https://api.spykemobile.net/SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddCustomerAttributes
	
	public static final String getSession = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AuthenticatePartner";
	public static final String FindCustomer = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/FindCustomer";
	public static final String GetRegisteredCards = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/GetRegisteredCards";
	public static final String EnrollCustomer = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/EnrollCustomer";
	public static final String AddCustomerAttributes = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddCustomerAttributes";
	public static final String AddOrUpdateCard = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddOrUpdateCard";
	public static final String AddOrUpdateTokenizedCard  = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddOrUpdateTokenizedCard";
	public static final String DeleteCard = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/DeleteCard";
	public static final String AuthenticateOBO = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AuthenticateOBO";
	public static final String AddSessionAttributes  = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddSessionAttributes";
	public static final String UpdateAccount = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/UpdateAccount";
	public static final String GetTransactionHistory = endpoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/GetTransactionHistory";
	
	
	
}
