package com.addcel.ingo.bridge.client.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTransactionHistoryRequest {

	private String customerId;
	private String accountId;
	private Integer pageSize;
	private String  pagingTransactionReferenceNumber;
	
	public GetTransactionHistoryRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getPagingTransactionReferenceNumber() {
		return pagingTransactionReferenceNumber;
	}
	public void setPagingTransactionReferenceNumber(
			String pagingTransactionReferenceNumber) {
		this.pagingTransactionReferenceNumber = pagingTransactionReferenceNumber;
	}
	
	
}
