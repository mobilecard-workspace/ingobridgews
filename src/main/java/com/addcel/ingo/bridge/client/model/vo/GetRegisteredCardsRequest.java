package com.addcel.ingo.bridge.client.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRegisteredCardsRequest {

	private String customerId;

	public GetRegisteredCardsRequest (){
		
	}
	
	
	/**
	 * @param customerId
	 */
	public GetRegisteredCardsRequest(String customerId) {
		this.customerId = customerId;
	}
	

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	
}
