package com.addcel.ingo.bridge.client.model.vo;

import java.util.ArrayList;
import java.util.List;

public class CardsResponse {

	private Integer errorCode;
	private String errorMessage;
	private List<Card> cards = new ArrayList<Card>();
	
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public List<Card> getCards() {
		return cards;
	}
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
}
