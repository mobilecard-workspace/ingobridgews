package com.addcel.ingo.bridge.client.model.vo;

public class AuthenticatePartnerResponse {

	private Integer errorCode;
	private String errorMessage;
	private CustomerInfo customerInfo;
	private Boolean hasTransactionsInFrankingPendingState;
	private Boolean hasTransactionsInReview;
	private Boolean isAuthenticated;
	private String partnerId;
	private String partnerName;
	private String sessionId;
	private Boolean showPrivacyPolicy;
	private Boolean showTermsAndConditions;
	private String termsToShow;
	
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}
	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}
	public Boolean getHasTransactionsInFrankingPendingState() {
		return hasTransactionsInFrankingPendingState;
	}
	public void setHasTransactionsInFrankingPendingState(
			Boolean hasTransactionsInFrankingPendingState) {
		this.hasTransactionsInFrankingPendingState = hasTransactionsInFrankingPendingState;
	}
	public Boolean getHasTransactionsInReview() {
		return hasTransactionsInReview;
	}
	public void setHasTransactionsInReview(Boolean hasTransactionsInReview) {
		this.hasTransactionsInReview = hasTransactionsInReview;
	}
	public Boolean getIsAuthenticated() {
		return isAuthenticated;
	}
	public void setIsAuthenticated(Boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Boolean getShowPrivacyPolicy() {
		return showPrivacyPolicy;
	}
	public void setShowPrivacyPolicy(Boolean showPrivacyPolicy) {
		this.showPrivacyPolicy = showPrivacyPolicy;
	}
	public Boolean getShowTermsAndConditions() {
		return showTermsAndConditions;
	}
	public void setShowTermsAndConditions(Boolean showTermsAndConditions) {
		this.showTermsAndConditions = showTermsAndConditions;
	}
	public String getTermsToShow() {
		return termsToShow;
	}
	public void setTermsToShow(String termsToShow) {
		this.termsToShow = termsToShow;
	}
	
}
