package com.addcel.ingo.bridge.client.model.vo;

//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionRequest {

	private String partnerConnectId;
	private String partnerConnectToken;
	
	public SessionRequest(){
		
	}
	
	public SessionRequest(String partnerConnectId, String partnerConnectToken) {
		this.partnerConnectId = partnerConnectId;
		this.partnerConnectToken = partnerConnectToken;
	}
	public String getPartnerConnectId() {
		return partnerConnectId;
	}
	public void setPartnerConnectId(String partnerConnectId) {
		this.partnerConnectId = partnerConnectId;
	}
	public String getPartnerConnectToken() {
		return partnerConnectToken;
	}
	public void setPartnerConnectToken(String partnerConnectToken) {
		this.partnerConnectToken = partnerConnectToken;
	}
	
	
	
}
