package com.addcel.ingo.bridge.client;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.ingo.bridge.client.model.vo.AddCustomerAttributesRequest;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateTokenizedCardRequest;
import com.addcel.ingo.bridge.client.model.vo.AddSessionAttributesRequest;
import com.addcel.ingo.bridge.client.model.vo.AuthenticateOBORequest;
import com.addcel.ingo.bridge.client.model.vo.AuthenticateOBOResponse;
import com.addcel.ingo.bridge.client.model.vo.AuthenticatePartnerResponse;
import com.addcel.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.ingo.bridge.client.model.vo.DeleteCardRequest;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.ingo.bridge.client.model.vo.GetTransactionHistoryRequest;
import com.addcel.ingo.bridge.client.model.vo.GetTransactionHistoryResponse;
import com.addcel.ingo.bridge.client.model.vo.Header;
import com.addcel.ingo.bridge.client.model.vo.LoginRequest;
import com.addcel.ingo.bridge.client.model.vo.Response;
import com.addcel.ingo.bridge.client.model.vo.SessionRequest;
import com.addcel.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.ingo.bridge.client.model.vo.UpdateAccountRequest;
import com.addcel.ingo.bridge.utils.Constants;
import com.addcel.ingo.bridge.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class INGORequest {

	
	@Autowired
	private UtilsService jsonUtils;
	@Autowired
	private RestTemplate restTemplate;

	
	/*public AuthenticatePartnerResponse getSession(String json){
		
		try{
			
			SessionRequest session = (SessionRequest)jsonUtils.jsonToObject(json,SessionRequest.class);
			Header header = (Header)jsonUtils.jsonToObject(json, Header.class);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("deviceId", header.getDeviceId());
			
			HttpEntity<SessionRequest> request = new HttpEntity<SessionRequest>(session,headers);

			AuthenticatePartnerResponse responseObject  =  (AuthenticatePartnerResponse)  restTemplate.postForObject( ServicesEndPoint.getSession, request, AuthenticatePartnerResponse.class);
			return responseObject;
		
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}*/
	
	//sstoken
	//custumer_id
	//session_id
	private SessionResponse getSessionData(LoginRequest login){
		try{
			SessionResponse response =  new SessionResponse();
			
			String session = getSession(new SessionRequest("Api.ConnectId.Android.050516.192907@chexar.com", "YzAxNzBkMGYtZTNjOS00YjM2LTg2ZjUtOTNkMWFkOWI2MmZk"), login.getDeviceId()).getSessionId();
			response.setSessionId(session);
			
			//traer de base de datos
			String customerId = "9f60d6b0-b2ba-4508-978e-0171f1f13f6d";
			response.setCustomerId(customerId);
			
			AuthenticateOBORequest authenticateOBORequest = new AuthenticateOBORequest();
			authenticateOBORequest.setCustomerId(customerId);
			AuthenticateOBOResponse  authenticateOBOResponse  = AuthenticateOBO(authenticateOBORequest, login.getDeviceId());
			response.setSsoToken(authenticateOBOResponse.getSsoToken());
			response.setErrorCode(authenticateOBOResponse.getErrorCode());
			response.setErrorMessage(authenticateOBOResponse.getErrorMessage());
			
			return response;
			
		}catch(Exception  ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	private HttpHeaders getHeader(String deviceId, String  sessionId){
		
		try{
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
				
			if(deviceId != null)
				headers.add("deviceId", deviceId);
			
			if(sessionId != null)
				headers.add("sessionId", sessionId);
			
			return headers;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
			
	}
	
public AuthenticatePartnerResponse getSession(SessionRequest session, String deviceId ){
		
		try{
			
			HttpHeaders headers = getHeader(deviceId,null);//new HttpHeaders();
			HttpEntity<SessionRequest> request = new HttpEntity<SessionRequest>(session,headers);

			AuthenticatePartnerResponse responseObject  =  (AuthenticatePartnerResponse)  restTemplate.postForObject( ServicesEndPoint.getSession, request, AuthenticatePartnerResponse.class);
			return responseObject;
		
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}
	
	public CustomerResponse FindCustomer(FindCustomerRequest findCustomerRequest, String deviceId){
		try{
			
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId(); //"Api.ConnectId.Android.050516.192907@chexar.com", "YzAxNzBkMGYtZTNjOS00YjM2LTg2ZjUtOTNkMWFkOWI2MmZk"
			HttpHeaders headers = getHeader(deviceId,session);//new HttpHeaders();
			//headers.setContentType(MediaType.APPLICATION_JSON);
			
			//json = AddcelCrypto.decryptSensitive(json);
			//FindCustomerRequest findCustomerRequest = (FindCustomerRequest)jsonUtils.jsonToObject(json,FindCustomerRequest.class);
			//Header header = (Header)jsonUtils.jsonToObject(json, Header.class);
			
			//headers.add("deviceId", header.getDeviceId());
			//headers.add("sessionId", header.getSessionId());
			
			HttpEntity<FindCustomerRequest> request = new HttpEntity<FindCustomerRequest>(findCustomerRequest, headers); 
			
			CustomerResponse responseObject  =  (CustomerResponse)  restTemplate.postForObject(ServicesEndPoint.FindCustomer , request, CustomerResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public CardsResponse GetRegisteredCards(GetRegisteredCardsRequest findCustomerRequest,String deviceId){
		try{
			
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);//new HttpHeaders();
			
			HttpEntity<GetRegisteredCardsRequest> request = new HttpEntity<GetRegisteredCardsRequest>(findCustomerRequest, headers); 
			
			CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.GetRegisteredCards , request, CardsResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public CustomerResponse EnrollCustomer(EnrollCustomerRequest enrollCustomerRequest,String deviceId){
		try{
					CustomerResponse responseObject = new CustomerResponse();
					FindCustomerRequest customer = new FindCustomerRequest();
					customer.setDateOfBirth(enrollCustomerRequest.getDateOfBirth());
					customer.setSsn(enrollCustomerRequest.getSsn());
					CustomerResponse customerR = FindCustomer(customer, deviceId);
					if(customerR.getErrorCode() != 0 ){
						String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
						
						HttpHeaders headers = getHeader(deviceId, session);
						
						HttpEntity<EnrollCustomerRequest> request = new HttpEntity<EnrollCustomerRequest>(enrollCustomerRequest, headers); 
						
						responseObject  =  (CustomerResponse)  restTemplate.postForObject(ServicesEndPoint.EnrollCustomer , request, CustomerResponse.class);
						
					}
					else
					{
						responseObject.setCustomerId(customerR.getCustomerId());
						responseObject.setErrorCode(0);
						responseObject.setErrorMessage("");
					}
					return responseObject;
					
				}catch(Exception ex){
					ex.printStackTrace();
					return null;
				}
	}
	
	public Response AddCustomerAttributes(AddCustomerAttributesRequest addCustomerAttributesRequest, String deviceId){
		
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<AddCustomerAttributesRequest> request = new HttpEntity<AddCustomerAttributesRequest>(addCustomerAttributesRequest, headers);
			
			Response responseObject  =  (Response)  restTemplate.postForObject(ServicesEndPoint.AddCustomerAttributes , request, Response.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
	}
	
	public CardsResponse AddOrUpdateCard(AddOrUpdateCardRequest addOrUpdateCardRequest,String deviceId){
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<AddOrUpdateCardRequest> request = new HttpEntity<AddOrUpdateCardRequest>(addOrUpdateCardRequest, headers);
			
			CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.AddOrUpdateCard , request, CardsResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public CardsResponse AddOrUpdateTokenizedCard(AddOrUpdateTokenizedCardRequest addOrUpdateTokenizedCardRequest, String deviceId){
		try{
			
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<AddOrUpdateTokenizedCardRequest> request = new HttpEntity<AddOrUpdateTokenizedCardRequest>(addOrUpdateTokenizedCardRequest, headers);
			
			CardsResponse responseObject  =  (CardsResponse)  restTemplate.postForObject(ServicesEndPoint.AddOrUpdateTokenizedCard , request, CardsResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public Response DeleteCard(DeleteCardRequest deleteCardRequest, String deviceId){
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<DeleteCardRequest> request = new HttpEntity<DeleteCardRequest>(deleteCardRequest, headers);
			
			Response responseObject  =  (Response)  restTemplate.postForObject(ServicesEndPoint.DeleteCard , request, Response.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public AuthenticateOBOResponse AuthenticateOBO (AuthenticateOBORequest authenticateOBORequest, String deviceId){
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<AuthenticateOBORequest> request = new HttpEntity<AuthenticateOBORequest>(authenticateOBORequest, headers);
			
			AuthenticateOBOResponse responseObject  =  (AuthenticateOBOResponse)  restTemplate.postForObject(ServicesEndPoint.AuthenticateOBO , request, AuthenticateOBOResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public Response AddSessionAttributes (AddSessionAttributesRequest addSessionAttributesRequest, String deviceId){
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<AddSessionAttributesRequest> request = new HttpEntity<AddSessionAttributesRequest>(addSessionAttributesRequest, headers);
			
			Response responseObject  =  (Response)  restTemplate.postForObject(ServicesEndPoint.AddSessionAttributes , request, Response.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public Response UpdateAccount (UpdateAccountRequest updateAccountRequest, String deviceId){
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<UpdateAccountRequest> request = new HttpEntity<UpdateAccountRequest>(updateAccountRequest, headers);
			
			Response responseObject  =  (Response)  restTemplate.postForObject(ServicesEndPoint.UpdateAccount , request, Response.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public GetTransactionHistoryResponse GetTransactionHistory(GetTransactionHistoryRequest getTransactionHistoryRequest, String deviceId){
		try{
			String session = getSession(new SessionRequest(Constants.connectId_Android,Constants.token_Android), deviceId).getSessionId();
			
			HttpHeaders headers = getHeader(deviceId, session);
			
			HttpEntity<GetTransactionHistoryRequest> request = new HttpEntity<GetTransactionHistoryRequest>(getTransactionHistoryRequest, headers);
			
			GetTransactionHistoryResponse responseObject  =  (GetTransactionHistoryResponse)  restTemplate.postForObject(ServicesEndPoint.GetTransactionHistory , request, GetTransactionHistoryResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	
	
	/*public void sendRequest(String json, Header headers, FindCustomerRequest object){ 
		try{
			
		
		HttpEntity<?> request = new HttpEntity<>(object,headers); 
	    //t = restTemplate.postForObject(ServicesEndPoint.FindCustomer , request, object.getClass());
	   // return (t);
		}catch(Exception ex){
			//return null;
		}
	}*/
	
	
}
