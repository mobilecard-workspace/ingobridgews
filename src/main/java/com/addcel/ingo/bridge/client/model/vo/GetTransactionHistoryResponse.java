package com.addcel.ingo.bridge.client.model.vo;

import java.util.ArrayList;
import java.util.List;

public class GetTransactionHistoryResponse {
	
	private Long errorCode;
	private String errorMessage;
	private List<SearchResult> searchResults = new ArrayList<SearchResult>();
	private Long totalRecords;
	
	public Long getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public List<SearchResult> getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(List<SearchResult> searchResults) {
		this.searchResults = searchResults;
	}
	public Long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}
	
	

}
