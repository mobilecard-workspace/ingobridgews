package com.addcel.ingo.bridge.client.model.vo;

public class Address {

	private Object addressLine1;
	private Object addressLine2;
	private Object city;
	private Object state;
	private Object zipCode;
	
	public Object getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(Object addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public Object getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(Object addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public Object getCity() {
		return city;
	}
	public void setCity(Object city) {
		this.city = city;
	}
	public Object getState() {
		return state;
	}
	public void setState(Object state) {
		this.state = state;
	}
	public Object getZipCode() {
		return zipCode;
	}
	public void setZipCode(Object zipCode) {
		this.zipCode = zipCode;
	}
}
