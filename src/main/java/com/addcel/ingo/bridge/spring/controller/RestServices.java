package com.addcel.ingo.bridge.spring.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.ingo.bridge.client.INGORequest;
import com.addcel.ingo.bridge.client.model.vo.AddCustomerAttributesRequest;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateTokenizedCardRequest;
import com.addcel.ingo.bridge.client.model.vo.AddSessionAttributesRequest;
import com.addcel.ingo.bridge.client.model.vo.AuthenticateOBORequest;
import com.addcel.ingo.bridge.client.model.vo.AuthenticateOBOResponse;
import com.addcel.ingo.bridge.client.model.vo.AuthenticatePartnerResponse;
import com.addcel.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.ingo.bridge.client.model.vo.DeleteCardRequest;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.FindCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.ingo.bridge.client.model.vo.GetTransactionHistoryRequest;
import com.addcel.ingo.bridge.client.model.vo.GetTransactionHistoryResponse;
import com.addcel.ingo.bridge.client.model.vo.Response;
import com.addcel.ingo.bridge.client.model.vo.SessionRequest;
import com.addcel.ingo.bridge.client.model.vo.SessionResponse;
import com.addcel.ingo.bridge.client.model.vo.UpdateAccountRequest;
import com.addcel.ingo.bridge.utils.Constants;


@Controller
public class RestServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServices.class);
	private static final String REQ_PARAM_JSON = "json";
	
	/*@Autowired
	private ServiceMapper mapper;*/
	
	@Autowired
	private INGORequest request;
	
	
	
	/*@RequestMapping(value = "/getSession", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody  AuthenticatePartnerResponse getSession() {
		
		 String jsonStringUser = "{"
			        + "\"partnerConnectId\":\"Api.ConnectId.Android.050516.192907@chexar.com\"" + ","
			        + "\"partnerConnectToken\":\"YzAxNzBkMGYtZTNjOS00YjM2LTg2ZjUtOTNkMWFkOWI2MmZk\"" + ","
			        + "\"deviceId\":\"1234567890\""
			        + "}";
		 
		return request.getSession(jsonStringUser);
 
    }*/
	
	
	
	/*@RequestMapping(value = "/getSession", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<AuthenticatePartnerResponse> update(@RequestBody SessionRequest s, @RequestHeader HttpHeaders httpHeaders) {
		 
		return new ResponseEntity<AuthenticatePartnerResponse>(request.getSession(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
	}*/
	
	@RequestMapping(value = "/getSession", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<AuthenticatePartnerResponse> update(@RequestHeader HttpHeaders httpHeaders) {
		String plataforma = httpHeaders.get("plataforma").get(0);
		LOGGER.debug("Plataforma: " + plataforma);
		String connectId = plataforma.equalsIgnoreCase("ANDROID") ? Constants.connectId_Android : Constants.connectId_Ios; // "Api.ConnectId.Android.050516.192907@chexar.com" : "Api.ConnectId.IOS.050516.192907@chexar.com";
		String token = plataforma.equalsIgnoreCase("ANDROID") ? Constants.token_Android : Constants.token_Ios;//"YzAxNzBkMGYtZTNjOS00YjM2LTg2ZjUtOTNkMWFkOWI2MmZk" : "NDRhY2E5MGItZTE4My00YjVjLWIzZTQtZGE4MDZhODI0NTZi";
		
		SessionRequest s = new SessionRequest(connectId,token);
		return new ResponseEntity<AuthenticatePartnerResponse>(request.getSession(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/FindCustomer", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CustomerResponse>  FindCustomer(@RequestBody FindCustomerRequest s, @RequestHeader HttpHeaders httpHeaders){ //@RequestParam(REQ_PARAM_JSON) String jsonEnc
		
		return new ResponseEntity<CustomerResponse>(request.FindCustomer(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		//return request.FindCustomer(jsonStringUser);
	}
	
	@RequestMapping(value="/GetRegisteredCards", method = RequestMethod.POST, produces = "application/json")
	public  ResponseEntity<CardsResponse>  GetRegisteredCards(@RequestBody GetRegisteredCardsRequest s, @RequestHeader HttpHeaders httpHeaders) {
		
		return new ResponseEntity<CardsResponse>(request.GetRegisteredCards(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
	}
	
	@RequestMapping(value="/EnrollCustomer", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CustomerResponse>  EnrollCustomer(@RequestBody EnrollCustomerRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		//{"errorCode":0,"errorMessage":"","customerId":"fa101903-1bb8-4d0a-8ef2-4ec2ccf8aafc"}
		//{"errorCode":0,"errorMessage":"","customerId":"4a7eb216-0f50-42b2-8c0a-455009a18d0d"}
		//"customerId": "9f60d6b0-b2ba-4508-978e-0171f1f13f6d"
		return new ResponseEntity<CustomerResponse>(request.EnrollCustomer(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(value = "/AddCustomerAttributes", method = RequestMethod.POST, produces = "application/json" )
	public ResponseEntity<Response>  AddCustomerAttributes(@RequestBody AddCustomerAttributesRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		return new ResponseEntity<Response>(request.AddCustomerAttributes(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		//return request.AddCustomerAttributes(jsonStringUser);
	}
	
	
	@RequestMapping(value = "/AddOrUpdateCard", method = RequestMethod.POST, produces =  "application/json")
	public  ResponseEntity<CardsResponse>  AddOrUpdateCard(@RequestBody AddOrUpdateCardRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		return new ResponseEntity<CardsResponse>(request.AddOrUpdateCard(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		 /**
		  * {"errorCode":0,"errorMessage":"","cards":[{"cardArtAsBase64Png":"","cardId":"535c03a4-e725-49c9-935e-ec7d5093851d","cardNickname":"","cardProgram":"","cardProgramPhone":"","currentCardMax":0,"currentCardMin":0,"customerId":"","expirationMonthYear":"","hashId":"","issuerType":0,"lastFourDigits":"2209","showTermsAndConditions":false,"termsAndConditionsId":""}]}
		  */
	}
	
	@RequestMapping(value = "/AddOrUpdateTokenizedCard", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CardsResponse>  AddOrUpdateTokenizedCard(@RequestBody AddOrUpdateTokenizedCardRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		return new ResponseEntity<CardsResponse>(request.AddOrUpdateTokenizedCard(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		 //return request.AddOrUpdateTokenizedCard(jsonStringUser);
	}
	
	@RequestMapping(value = "DeleteCard", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Response>  DeleteCard(@RequestBody DeleteCardRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		return new ResponseEntity<Response>(request.DeleteCard(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		// return request.DeleteCard(jsonStringUser);
	}
	
	@RequestMapping(value = "AuthenticateOBO", method = RequestMethod.POST, produces = "application/json")
	public  ResponseEntity<AuthenticateOBOResponse>   AuthenticateOBO(@RequestBody AuthenticateOBORequest s, @RequestHeader HttpHeaders httpHeaders){
		
		  return new ResponseEntity<AuthenticateOBOResponse>(request.AuthenticateOBO(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		 //return request.AuthenticateOBO(jsonStringUser);	 
	}
	
	@RequestMapping(value = "AddSessionAttributes", method = RequestMethod.POST, produces = "application/json")
	public  ResponseEntity<Response>   AddSessionAttributes(@RequestBody AddSessionAttributesRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		return new ResponseEntity<Response>(request.AddSessionAttributes(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		// return request.AddSessionAttributes(jsonStringUser);	 
	}
	
	@RequestMapping(value = "UpdateAccount", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Response>  UpdateAccount(@RequestBody UpdateAccountRequest s, @RequestHeader HttpHeaders httpHeaders){
		
		return new ResponseEntity<Response>(request.UpdateAccount(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		// return request.UpdateAccount(jsonStringUser);	 
	}
	
	@RequestMapping(value = "GetTransactionHistory", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<GetTransactionHistoryResponse>  GetTransactionHistory(@RequestBody GetTransactionHistoryRequest s, @RequestHeader HttpHeaders httpHeaders){
		return new ResponseEntity<GetTransactionHistoryResponse>(request.GetTransactionHistory(s,httpHeaders.get("deviceId").get(0)), HttpStatus.OK);
		// return request.GetTransactionHistory(jsonStringUser);	 
	}
	

	@RequestMapping(value = "/home", method=RequestMethod.GET)
	public ModelAndView getToken() {
		
		ModelAndView mav = new ModelAndView("home");
		return mav;
	}
	
	

}

